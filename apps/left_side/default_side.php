<aside id="left-panel" class="left-panel">
    <nav class="navbar navbar-expand-sm navbar-default">

        <div class="navbar-header">
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#main-menu" aria-controls="main-menu" aria-expanded="false" aria-label="Toggle navigation">
                <i class="fa fa-bars"></i>
            </button>
            <a class="navbar-brand" href="./"><img src="images/logo.png" alt="Pencatatan"></a>
            <a class="navbar-brand hidden" href="./"><img src="images/logo2.png" alt="P"></a>
        </div>

        <div id="main-menu" class="main-menu collapse navbar-collapse">
            <ul class="nav navbar-nav">
                <?php 
                    /*switch ((isset($_GET['contain']) ? $_GET['contain'] : '')) {
                        case 'penjualan':
                            include "navbar/navbar_menu.php";
                            include "contain/submit_penjualan.php";
                            break;
                        case 'pemesanan_obat':
                            include "navbar/navbar_menu.php";
                            include "contain/submit_pemesanan.php";
                            break;
                        default:
                            include "navbar/navbar_default.php";
                            break;
                    }*/
                ?>
                <li class="<?php if ($_GET['contain']==null){echo"active";} ?>">
                    <a href="index.php"> <i class="menu-icon fa fa-dashboard"></i>Dashboard </a>
                </li>
                <h3 class="menu-title">Laporan</h3>
                
                <li class="<?php if ($_GET['contain']=='laporan_btl'){echo"active";} ?>">
                    <a href="index.php?contain=laporan_btl"> <i class="menu-icon fa fa-table"></i>Laporan BTL</a>
                </li>
                <li class="<?php if ($_GET['contain']=='laporan_realisasi'){echo"active";} ?>">
                    <a href="index.php?contain=laporan_realisasi"> <i class="menu-icon fa fa-table"></i>Laporan Realisasi</a>
                </li>

                <h3 class="menu-title">Master</h3>
                <li class="<?php if ($_GET['contain']=='master_anggaran'){echo"active";} ?>">
                    <a href="index.php?contain=master_anggaran"> <i class="menu-icon fa fa-calendar"></i>Data Anggaran</a>
                </li>
                <li class="<?php if ($_GET['contain']=='master_realisasi'){echo"active";} ?>">
                    <a href="index.php?contain=master_realisasi"> <i class="menu-icon fa fa-calendar"></i>Data Realisasi</a>
                </li>

                <h3 class="menu-title"></h3>
                <li class="<?php if ($_GET['contain']==''){echo"active";} ?>">
                    <a href="../include/logout.php"> <i class="menu-icon fa fa-sign-out"></i>Logout</a>
                </li>
                <!-- <li class="menu-item-has-children dropdown <?php if ($_GET['contain']=='master_wp'){echo"active";} ?>">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-bars"></i>Data WP</a>
                    <ul class="sub-menu children dropdown-menu">
                        <li><i class="fa fa-cutlery"></i><a href="index.php?contain=master_wp">Restoran</a></li>
                        <li><i class="fa fa-building-o"></i><a href="">Hotel</a></li>
                    </ul>
                </li>
                <li class="<?php if ($_GET['contain']=='master_jenis_pajak'){echo"active";} ?>">
                    <a href="index.php?contain=master_jenis_pajak"> <i class="menu-icon fa fa-flag"></i>Jenis Pajak </a>
                </li> -->
            </ul>
        </div><!-- /.navbar-collapse -->
    </nav>
</aside><!-- /#left-panel -->