<!DOCTYPE html>
<?php
    session_start(); 
    if(isset($_SESSION['id'])){
?>
<html class="no-js" lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Sistem Pencatatan</title>
    <meta name="description" content="Sebuah Sistem Pencatatan">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="apple-touch-icon" href="../apple-icon.png">
    <link rel="shortcut icon" href="../images/icon/favicon.ico">

    <link rel="stylesheet" href="../vendors/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="../vendors/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="../vendors/themify-icons/css/themify-icons.css">
    <link rel="stylesheet" href="../vendors/flag-icon-css/css/flag-icon.min.css">
    <link rel="stylesheet" href="../vendors/selectFX/css/cs-skin-elastic.css">
    <link rel="stylesheet" href="../vendors/jqvmap/dist/jqvmap.min.css">
    
    <!-- <link rel="stylesheet" href="../assets/css/bootstrap-select.min.css"> -->


    <!-- ============ -->
    <!--  Data Tables -->
    <!-- ============ -->
    <link rel="stylesheet" href="../vendors/datatables.net-bs4/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" href="../vendors/datatables.net-buttons-bs4/css/buttons.bootstrap4.min.css">
    <!-- ============ -->
    <!-- End Data Tables -->
    <!-- ============ -->

    <link rel="stylesheet" href="../assets/css/style.css">

    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800' rel='stylesheet' type='text/css'>

</head>

<body>
    <!-- Left Panel -->
    <?php
        include "left_side/default_side.php";
    ?>
    <!-- Left Panel -->

    <!-- Right Panel -->

    <div id="right-panel" class="right-panel">

        <!-- Header-->
        <header id="header" class="header">

            <div class="header-menu">

                <div class="col-sm-7">
                    <a id="menuToggle" class="menutoggle pull-left"><i class="fa fa fa-tasks"></i></a>
                </div>

                <div class="col-sm-5">
                    <div class="user-area dropdown float-right">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <h3><?php echo ucwords($_SESSION['nama_skpd']); ?> <i class="fa fa-angle-down"></i></h3>
                        </a>

                        <div class="user-menu dropdown-menu">
                            <a class="nav-link" href="#"><i class="fa fa-user"></i> My Profile</a>
                            <a class="nav-link" href="../include/logout.php"><i class="fa fa-power-off"></i> Logout</a>
                        </div>
                    </div>
                </div>

            </div>

        </header><!-- /header -->
        <!-- End Header-->

        <!-- Contain -->
        <?php 

        switch ((isset($_GET['contain']) ? $_GET['contain'] : '')) {
            case 'dashboard':
                include "contain/dashboard.php";
                break;
                
            case 'laporan_btl':
                include "contain/laporan_btl.php";
                break;
            case 'laporan_realisasi':
                include "contain/laporan_realisasi.php";
                break;
                
            case 'master_anggaran':
                include "contain/master_anggaran.php";
                break;
                
            case 'master_pengguna':
                include "contain/master_pengguna.php";
                break;
            case 'master_realisasi':
                include "contain/master_realisasi.php";
                break;
            case 'edit_realisasi':
                include "contain/edit_realisasi.php";
                break;

            case 'master_tahun':
                include "contain/master_tahun.php";
                break;
            case 'konfirmasi_tahun':
                include "contain/konfirmasi_tahun.php";
                break;
            case 'setting_gaji':
                include "contain/setting_gaji.php";
                break;
            case 'konfirmasi_gaji':
                include "contain/konfirmasi_gaji.php";
                break;
                
            default:
                include "contain/dashboard.php";
                break;
        }

        ?>
        <!-- End Contain -->


    </div><!-- /#right-panel -->

    <!-- Right Panel -->
    <script src="../vendors/jquery/dist/jquery.min.js"></script>
    <script src="../vendors/popper.js/dist/umd/popper.min.js"></script>
    <script src="../vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="../assets/js/main.js"></script>


    <script src="../vendors/chart.js/dist/Chart.bundle.min.js"></script>
    <script src="../assets/js/dashboard.js"></script>
    <script src="../assets/js/widgets.js"></script>
    <script src="../vendors/jqvmap/dist/jquery.vmap.min.js"></script>
    <script src="../vendors/jqvmap/examples/js/jquery.vmap.sampledata.js"></script>
    <script src="../vendors/jqvmap/dist/maps/jquery.vmap.world.js"></script>


    <!-- ============ -->
    <!--  Data Tables -->
    <!-- ============ -->
    <script src="../vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="../vendors/datatables.net-bs4/js/dataTables.bootstrap4.min.js"></script>
    <script src="../vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="../vendors/datatables.net-buttons-bs4/js/buttons.bootstrap4.min.js"></script>
    <script src="../vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="../vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="../vendors/datatables.net-buttons/js/buttons.colVis.min.js"></script>
    <script src="../assets/js/init-scripts/data-table/datatables-init.js"></script>

    <!-- <script src="../assets/js/bootstrap-select.min.js"></script> -->
    <!-- ============ -->
    <!-- End Data Tables -->
    <!-- ============ -->

    <script>
        (function($) {
            "use strict";

            jQuery('#vmap').vectorMap({
                map: 'world_en',
                backgroundColor: null,
                color: '#ffffff',
                hoverOpacity: 0.7,
                selectedColor: '#1de9b6',
                enableZoom: true,
                showTooltip: true,
                values: sample_data,
                scaleColors: ['#1de9b6', '#03a9f5'],
                normalizeFunction: 'polynomial'
            });
        })(jQuery);
    </script>

</body>
</html>
<?php
}else{
    echo '<script languange="javascript">alert ("Silahkan login terlebih dahulu")</script>';
    echo '<script languange="javascript">window.location="../index.php"</script>';
}
?>