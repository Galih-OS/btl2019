<div class="breadcrumbs">
    <div class="col-sm-4">
        <div class="page-header float-left">
            <div class="page-title">
                <h1>Master Anggaran</h1>
            </div>
        </div>
    </div>
    <div class="col-sm-8">
        <div class="page-header float-right">
        </div>
    </div>
</div>

<div class="content mt-3">
    <div class="animated fadeIn">
        <div class="row">

            <?php include "../include/connect.php"; ?>

            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <div class="float-left">
                            <strong class="card-title">Data Anggaran</strong>
                        </div>
                        <div class="float-right">
                            <!-- <a class="btn btn-primary" href="index.php?contain=tambah_wp" role="button"><i class="fa fa-plus"></i>&nbsp; Tambah Tahun</a> -->
                        </div>
                    </div>

                    <div class="card-body" style="overflow-x:auto;">
                                <!-- TABLE DATA -->
                                <!-- TABLE DATA -->
                                <form action="" method="post" enctype="multipart/form-data">
                                    <table id="bootstrap-data-table-export" class="table table-striped table-hover table-bordered">
                                        <thead>
                                            <tr align="center">
                                                <th>Nama SKPD</th>
                                                <th>Kode Rincian</th>
                                                <th>Nama Rincian</th>
                                                <th>Anggaran</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        <?php 
                                            foreach($db->query('SELECT nama_skpd, rincian.id_rincian as id_rincian, anggaran, nama_rincian FROM btl
                                                                JOIN skpd ON skpd.id_skpd = btl.id_skpd
                                                                JOIN rincian ON rincian.id_rincian = btl.id_rincian
                                                                JOIN tahun ON tahun.id_tahun = btl.id_tahun
                                                                WHERE skpd.nama_skpd = "'.$_SESSION['nama_skpd'].'" AND tahun.status_tahun = "Aktif" ') as $row) {
                                        ?>
                                            <tr>
                                                <td align="center">
                                                    <?php echo $row['nama_skpd'];?>
                                                </td>
                                                <td align="center">
                                                    <?php echo $row['id_rincian'];?>
                                                </td>
                                                <td align="">
                                                    <?php echo $row['nama_rincian'];?>
                                                </td>
                                                <td align="right">
                                                    <?php echo number_format($row['anggaran']);?>
                                                </td>
                                            </tr>
                                        <?php
                                            }
                                        ?>
                                        </tbody>
                                    </table>
                                </form>
                                <!-- END TABLE DATA -->
                                <!-- END TABLE DATA -->
                    </div>
                </div>
            </div>


        </div>
    </div><!-- .animated -->
</div><!-- .content -->

<?php
    if(isset($_POST['hapus']))
    {
        $id_restoran = $_POST['id_restoran'];
        
        $stmt=$db->prepare("DELETE FROM restoran WHERE id_restoran = '".$id_restoran."'");
        $stmt->execute();

        
        if($stmt){
            echo '<script languange="javascript">window.alert("'.$stmt.'")</script>';
            //echo '<script languange="javascript">window.location="index.php?contain=master_wp"</script>';
        } else {
            //echo '<script languange="javascript">alert("'.$id_restoran.'")</script>';
        }

    }
?>