<div class="breadcrumbs">
    <div class="col-sm-4">
        <div class="page-header float-left">
            <div class="page-title">
                <h1>Master Tahun</h1>
            </div>
        </div>
    </div>
    <div class="col-sm-8">
        <div class="page-header float-right">
        </div>
    </div>
</div>

<div class="content mt-3">
    <div class="animated fadeIn">
        <div class="row">

            <?php include "../include/connect.php"; ?>

            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <div class="float-left">
                            <strong class="card-title">Data Tahun</strong>
                        </div>
                        <div class="float-right">
                            <!-- <a class="btn btn-primary" href="index.php?contain=tambah_wp" role="button"><i class="fa fa-plus"></i>&nbsp; Tambah Tahun</a> -->
                        </div>
                    </div>

                    <div class="card-body" style="overflow-x:auto;">

                    <!-- QUERY TAMBAH DATA -->
                    <!-- QUERY TAMBAH DATA -->
                    <?php
                            if(isset($_POST['tambah']))
                        {
                            $stmt = $db->prepare("SELECT MAX(id_tahun)+1 AS max FROM tahun");
                            $stmt -> execute();
                            $invNum = $stmt -> fetch(PDO::FETCH_ASSOC);
                            $max = $invNum['max'];
                            if ($max == null) {
                                $max = 1;
                            }

                            $tahun = $_POST['tahun'];
                            $status_tahun = 'Tidak Aktif';

                            $sql_insert = "INSERT INTO tahun (id_tahun,nama_tahun,status_tahun) VALUES (?,?,?)";
                            $stmt= $db->prepare($sql_insert);
                            $stmt->execute([$max, $tahun, $status_tahun]);

                            if ($stmt) {
                    ?>
                                <div class="alert  alert-success alert-dismissible fade show" role="alert">
                                    <span class="badge badge-pill badge-success">Sukses</span> Data Sukses Di Tambahkan.
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                    <?php
                            } else {
                    ?>
                                <div class="alert  alert-danger alert-dismissible fade show" role="alert">
                                    <span class="badge badge-pill badge-danger">Gagal</span> Data Gagal Di Tambahkan.
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                    <?php
                            }
                        }
                    ?>
                    <!-- END QUERY TAMBAH DATA -->
                    <!-- END QUERY TAMBAH DATA -->

                        <!-- FORM TAMBAH DATA -->
                        <!-- FORM TAMBAH DATA -->
                        <!-- FORM TAMBAH DATA -->
                        <form action="" method="post" enctype="multipart/form-data" class="form-horizontal">
                            <div class="row form-group" align="right">
                                <div class="col-2 col-md-2">
                                    <label for="text-input" class="form-control-label">Tahun</label>
                                </div>
                                <div class="col-10 col-md-6">
                                    <input type="text" id="text-input" name="tahun" placeholder="Input 4 Digit Angka untuk Tahun" class="form-control">
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col col-md-2">
                                </div>
                                <div class="col-12 col-md-6" align="right">
                                    <button type="submit" class="btn btn-primary btn-sm" name="tambah">
                                        <i class="fa fa-check"></i> Tambah
                                    </button>
                                    <button type="reset" class="btn btn-danger btn-sm">
                                        <i class="fa fa-times"></i> Reset
                                    </button>
                                </div>
                            </div>
                        </form>
                        <!-- END FORM TAMBAH DATA -->
                        <!-- END FORM TAMBAH DATA -->
                        <!-- END FORM TAMBAH DATA -->

                        <!-- TABLE DATA -->
                        <!-- TABLE DATA -->
                        <!-- TABLE DATA -->
                        <form action="" method="post" enctype="multipart/form-data">
                            <table id="bootstrap-data-table-export" class="table table-striped table-hover table-bordered">
                                <thead>
                                    <tr align="center">
                                        <th>Tahun</th>
                                        <th>Status Tahun</th>
                                        <th>Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php 
                                    foreach($db->query('SELECT * FROM tahun') as $row) {
                                ?>
                                    <tr>
                                        <td align="center">
                                            <?php echo $row['nama_tahun'];?>
                                        </td>
                                        <td align="center"><?php    if($row['status_tahun']=='Aktif'){
                                                                        echo '<span class="badge badge-pill badge-success">Aktif</span>';
                                                                    } else {
                                                                        echo '<span class="badge badge-pill badge-secondary">Tidak Aktif</span>';
                                                                    } ?>
                                        </td>
                                        <td align="center">
                                            <input type="text" name="id_tahun" value="<?php echo $row['id_tahun']; ?>" hidden>
                                            <?php 
                                                if($row['status_tahun']=='Aktif'){
                                                    /*echo '<button type="submit" class="btn btn-info btn-sm" name="edit">
                                                                <i class="fa fa-check-square-o"></i> Jadikan Tahun Aktif'.$row['id_tahun'].'
                                                            </button>';*/
                                                } else {
                                                    echo '<a class="btn btn-info btn-sm" href="index.php?contain=konfirmasi_tahun&id='.$row['id_tahun'].'" role="button"><i class="fa fa-edit"></i>&nbsp; Jadikan Tahun Aktif</a>';
                                                    /*echo '<button type="submit" class="btn btn-info btn-sm" name="edit">
                                                                <i class="fa fa-check-square-o"></i> Jadikan Tahun Aktif'.$row['id_tahun'].'
                                                            </button>';*/
                                                }
                                            ?>
                                            <!-- <button class="btn btn-danger btn-sm" onclick="return confirm('Menghapus data ini?');" name="hapus" type="submit"><i class="fa fa-times"></i>&nbsp; Hapus</button> -->
                                        </td>
                                    </tr>
                                <?php
                                    }
                                ?>
                                </tbody>
                            </table>
                        </form>
                        <!-- END TABLE DATA -->
                        <!-- END TABLE DATA -->
                        <!-- END TABLE DATA -->
                    </div>
                </div>
            </div>


        </div>
    </div><!-- .animated -->
</div><!-- .content -->

<?php
    if(isset($_POST['hapus']))
    {
        $id_restoran = $_POST['id_restoran'];
        
        $stmt=$db->prepare("DELETE FROM restoran WHERE id_restoran = '".$id_restoran."'");
        $stmt->execute();

        
        if($stmt){
            echo '<script languange="javascript">window.alert("'.$stmt.'")</script>';
            //echo '<script languange="javascript">window.location="index.php?contain=master_wp"</script>';
        } else {
            //echo '<script languange="javascript">alert("'.$id_restoran.'")</script>';
        }

    }
?>