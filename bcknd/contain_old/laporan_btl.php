<div class="breadcrumbs">
    <div class="col-sm-4">
        <div class="page-header float-left">
            <div class="page-title">
                <h1>Laporan</h1>
            </div>
        </div>
    </div>
    <div class="col-sm-8">
        <div class="page-header float-right">
        </div>
    </div>
</div>

<?php
    include "../include/connect.php";

    if (isset($_POST['cari'])) {
        $stmt = $db->prepare("SELECT nama_skpd AS nama FROM skpd WHERE id_skpd = '".$_POST['skpd']."' ");
        $stmt -> execute();
        $invNum = $stmt -> fetch(PDO::FETCH_ASSOC);
        $nama = $invNum['nama'];
    }

    function NamaBulan($num) {
        $NamaBulan [1] = 'Januari';
        $NamaBulan [2] = 'Februari';
        $NamaBulan [3] = 'Maret';
        $NamaBulan [4] = 'April';
        $NamaBulan [5] = 'Mei';
        $NamaBulan [6] = 'Juni';
        $NamaBulan [7] = 'Juli';
        $NamaBulan [8] = 'Agustus';
        $NamaBulan [9] = 'September';
        $NamaBulan [10] = 'Oktober';
        $NamaBulan [11] = 'November';
        $NamaBulan [12] = 'Desember';

        return $NamaBulan[$num];
    }

    function NamaRealisasi($num) {
        $NamaBulan [1] = 'realisasi_januari';
        $NamaBulan [2] = 'realisasi_februari';
        $NamaBulan [3] = 'realisasi_maret';
        $NamaBulan [4] = 'realisasi_april';
        $NamaBulan [5] = 'realisasi_mei';
        $NamaBulan [6] = 'realisasi_juni';
        $NamaBulan [7] = 'realisasi_juli';
        $NamaBulan [8] = 'realisasi_agustus';
        $NamaBulan [9] = 'realisasi_september';
        $NamaBulan [10] = 'realisasi_oktober';
        $NamaBulan [11] = 'realisasi_november';
        $NamaBulan [12] = 'realisasi_desember';

        return $NamaRealisasi[$num];
    }

    function NamaAbjad($num) {
        $NamaAbjad [1] = 'D';
        $NamaAbjad [2] = 'E';
        $NamaAbjad [3] = 'F';
        $NamaAbjad [4] = 'G';
        $NamaAbjad [5] = 'H';
        $NamaAbjad [6] = 'I';
        $NamaAbjad [7] = 'J';
        $NamaAbjad [8] = 'K';
        $NamaAbjad [9] = 'L';
        $NamaAbjad [10] = 'M';
        $NamaAbjad [11] = 'N';
        $NamaAbjad [12] = 'O';
        $NamaAbjad [13] = 'P';
        $NamaAbjad [14] = 'Q';
        $NamaAbjad [15] = 'R';
        $NamaAbjad [16] = 'S';
        $NamaAbjad [17] = 'T';
        $NamaAbjad [18] = 'U';

        return $NamaAbjad[$num];
    }
?>

<div class="content mt-3">
    <div class="animated fadeIn">
        <div class="row">

            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <div class="float-left">
                            <strong class="card-title">Laporan Belanja Tidak Langsung</strong>
                        </div>
                    </div>
                    <div class="card-body card-block" style="overflow-x:auto; height: 100%;">
                        <form action="" method="POST">
                            <div class="row form-group" align="right">
                                <div class="col-2 col-md-2">
                                    <label for="text-input" class="form-control-label">SKPD</label>
                                </div>
                                <div class="col-10 col-md-6">
                                    <select class="form-control" name="skpd" required>
                                <?php
                                    if (isset($_POST['cari'])) {
                                ?>
                                        <option selected value="<?php echo $_POST['skpd']; ?>"><?php echo $nama; ?></option>';
                                <?php
                                    } else {
                                        echo '<option disabled selected>- Nama SKPD -</option>';
                                    }
                                ?>
                                    <?php
                                        foreach($db->query("SELECT * FROM skpd ORDER BY nama_skpd ASC") as $row) {
                                    ?>
                                        <option value="<?php echo $row['id_skpd']; ?>"><?php echo $row['nama_skpd']; ?></option>
                                    <?php 
                                        }
                                    ?>
                                    </select>

                                </div>
                            </div>
                            <div class="row form-group" align="right">
                                <div class="col-2 col-md-2">
                                    <label for="text-input" class="form-control-label">Bulan</label>
                                </div>
                                <div class="col-10 col-md-6">
                                    <select class="form-control" name="bulan" required>
                                    <?php
                                        if (isset($_POST['cari'])) {
                                    ?>
                                            <option selected value="<?php echo $_POST['bulan']; ?>"><?php echo NamaBulan($_POST['bulan']); ?></option>
                                    <?php
                                        } else {
                                            echo '<option disabled selected>- Bulan -</option>';
                                        }
                                    ?>
                                        <option value="1">Januari</option>
                                        <option value="2">Februari</option>
                                        <option value="3">Maret</option>
                                        <option value="4">April</option>
                                        <option value="5">Mei</option>
                                        <option value="6">Juni</option>
                                        <option value="7">Juli</option>
                                        <option value="8">Agustus</option>
                                        <option value="9">September</option>
                                        <option value="10">Oktober</option>
                                        <option value="11">November</option>
                                        <option value="12">Desember</option>
                                    </select>
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col col-md-2">
                                </div>
                                <div class="col-12 col-md-6" align="right">
                                    <button type="submit" class="btn btn-primary btn-sm" name="cari">
                                        <i class="fa fa-search"></i> Cari
                                    </button>
                                </div>
                            </div>        
                        </form>

                        <?php
                            if (isset($_POST['cari'])) {
                        ?>
                                <br>
                                <strong class="card-title">
                                    <h3 align="center">Laporan Belanja Tidak Langsung<br/><?php echo $nama; ?></h3><br/>
                                </strong>

                                <table id="bootstrap-data-table-export" class="table table-striped table-hover table-bordered">
                                    
                                    <!-- CONTAIN HEAD -->
                                    <?php //include "atribut/table_head_btl.php"; ?>
                                    <!-- END CONTAIN HEAD -->

                                    <thead>
                                        <tr align="center">
                                            <th rowspan="2" valign="middle">No.</th>
                                            <th rowspan="2" valign="middle">Keterangan</th>
                                            <th rowspan="2" valign="middle">Anggaran</th>
                                            <th colspan="<?php echo $_POST['bulan']+1; ?>">Realisasi</th>
                                            <th rowspan="2" valign="middle">Prediksi</th>
                                            <th rowspan="2" valign="middle">Kenaikan Gaji</th>
                                            <th rowspan="2" valign="middle">Total</th>
                                            <th rowspan="2" valign="middle">Selisih</th>
                                        </tr>
                                        <tr align="center">
                                        <?php
                                            for ($i=1; $i <= $_POST['bulan']; $i++) { 
                                                echo "<th>".NamaBulan($i)."</th>";
                                            }
                                        ?>
                                            <th>Jumlah</th>
                                        </tr>
                                        <tr align="center" style="font-size:12px;">
                                            <th>A</th>
                                            <th>B</th>
                                            <th>C</th>
                                        <?php
                                            $abjadjumlah = "";
                                            for ($i=1; $i <= $_POST['bulan']; $i++) { 
                                                echo "<th>".NamaAbjad($i)."</th>";
                                                if ($i != $_POST['bulan']) {
                                                    $abjadjumlah .= NamaAbjad($i)."+";
                                                } else {
                                                    $abjadjumlah .= NamaAbjad($i);
                                                }
                                            }
                                            //Jumlah
                                            echo "<th>".NamaAbjad($_POST['bulan']+1)."=".$abjadjumlah."</th>";
                                            //Prediksi
                                            echo "<th>".NamaAbjad($_POST['bulan']+2)."=".NamaAbjad($_POST['bulan'])."x".(14-$_POST['bulan'])."</th>";
                                            //Kenaikan Gaji
                                            echo "<th>".NamaAbjad($_POST['bulan']+3)."=5%x".NamaAbjad($_POST['bulan'])."x14</th>";
                                            //Total
                                            echo "<th>".NamaAbjad($_POST['bulan']+4)."=".NamaAbjad($_POST['bulan']+1)."+".NamaAbjad($_POST['bulan']+2)."+".NamaAbjad($_POST['bulan']+3)."</th>";
                                            //Selisih
                                            echo "<th>".NamaAbjad($_POST['bulan']+5)."=C-".NamaAbjad($_POST['bulan']+4)."</th>";
                                        ?>
                                        </tr>
                                    </thead>                                    
                                    <tbody>
                                    <?php 
                                        $no = 1;
                                        foreach($db->query("SELECT * FROM btl
                                                                JOIN rincian ON rincian.id_rincian = btl.id_rincian
                                                                JOIN tahun ON tahun.id_tahun = btl.id_tahun
                                                                WHERE tahun.status_tahun = 'Aktif' AND
                                                                        btl.id_skpd = '".$_POST['bulan']."' ") as $row) {
                                    ?>
                                        <tr>
                                            <td align="center"><?php echo $row['id_rincian']; ?></td>
                                            <td align="left"><?php echo $row['nama_rincian']; ?></td>
                                            <td align="right"><?php echo $row['anggaran']; ?></td>

                                            <td align="right"><?php echo $row['realisasi_januari']; ?></td>
                                            <td align="right"><?php echo $row['realisasi_februari']; ?></td>
                                            <td align="right"><?php echo $row['realisasi_maret']; ?></td>
                                            <td align="right"><?php echo $row['realisasi_april']; ?></td>
                                            <td align="right"><?php echo $row['realisasi_mei']; ?></td>
                                            <td align="right"><?php echo $row['realisasi_juni']; ?></td>
                                            <td align="right"><?php echo $row['realisasi_juli']; ?></td>
                                            <td align="right"><?php echo $row['realisasi_agustus']; ?></td>
                                            <td align="right"><?php echo $row['realisasi_september']; ?></td>
                                            <td align="right"><?php echo $row['realisasi_oktober']; ?></td>
                                            <td align="right"><?php echo $row['realisasi_november']; ?></td>
                                            <td align="right"><?php echo $row['realisasi_desember']; ?></td>

                                    <?php
                                        $total=array($row['realisasi_januari'],$row['realisasi_februari'],$row['realisasi_maret'],
                                                $row['realisasi_april'],$row['realisasi_mei'],$row['realisasi_juni'],
                                                $row['realisasi_juli'],$row['realisasi_agustus'],$row['realisasi_september'],
                                                $row['realisasi_oktober'],$row['realisasi_november'],$row['realisasi_desember']);
                                    ?>
                                            <td align="right"><?php echo array_sum($total); ?></td>
                                            
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                    <?php
                                            $no += 1;
                                        }
                                    ?>
                                    </tbody>
                                </table>
                        <?php
                            }
                        ?>
                    </div>
                </div>
            </div>


        </div>
    </div><!-- .animated -->
</div><!-- .content -->