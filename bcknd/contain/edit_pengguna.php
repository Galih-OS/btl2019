<script>
function myFunction() {
  var x = document.getElementById("seepass");
  if (x.type === "password") {
    x.type = "text";
  } else {
    x.type = "password";
  }
}
</script>

<div class="breadcrumbs">
    <div class="col-sm-4">
        <div class="page-header float-left">
            <div class="page-title">
                <h1>Master Pengguna</h1>
            </div>
        </div>
    </div>
    <div class="col-sm-8">
        <div class="page-header float-right">
        </div>
    </div>
</div>

<div class="content mt-3">
    <div class="animated fadeIn">
        <div class="row">

            <?php include "../include/connect.php"; ?>

            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <div class="float-left">
                            <strong class="card-title">Edit Pengguna</strong>
                        </div>
                    </div>
                    <?php 
                        // Jika Sukses
                        /*<div class="alert  alert-success alert-dismissible fade show" role="alert">
                            <span class="badge badge-pill badge-success">Success</span> You successfully read this important alert message.
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>*/
                        if(isset($_POST['edit']))
                        {
                            $nama = strtolower($_POST['nama']);
                            $username = strtolower($_POST['username']);
                            $password = strtolower($_POST['password']);
                            $role = $_POST['role'];

                            $sql_update = $db->exec("UPDATE pengguna
                                                        SET nama_pengguna= '$nama', username= '$username', password= '$password',
                                                            role_pengguna= '$role'
                                                        WHERE id_pengguna = '".$_GET["id"]."' ");

                            if ($sql_update) {
                    ?>
                                <div class="alert  alert-success alert-dismissible fade show" role="alert">
                                    <span class="badge badge-pill badge-success">Sukses</span> Data Sukses Di Ubah.
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                    <?php
                            } else {
                    ?>
                                <div class="alert  alert-danger alert-dismissible fade show" role="alert">
                                    <span class="badge badge-pill badge-danger">Gagal</span> Data Gagal Di Ubah.
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                    <?php
                            }
                        }
                    ?>
                    <?php
                        foreach($db->query('SELECT * FROM pengguna WHERE id_pengguna = '.$_GET["id"].'') as $row) {
                    ?>
                    <div class="card-body card-block">
                        <form action="" method="post" enctype="multipart/form-data" class="form-horizontal">
                            <div class="row form-group">
                                <div class="col col-md-2">
                                    <label for="text-input" class="form-control-label">Nama Pengguna</label>
                                </div>
                                <div class="col-12 col-md-6">
                                    <input type="text" id="text-input" name="nama" value="<?php echo $row['nama_pengguna'] ?>" class="form-control" required>
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col col-md-2">
                                    <label for="text-input" class="form-control-label">Username</label>
                                </div>
                                <div class="col-12 col-md-6">
                                    <input type="text" id="text-input" name="username" value="<?php echo $row['username'] ?>" class="form-control" required>
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col col-md-2">
                                    <label for="text-input" class="form-control-label">Password</label>
                                </div>
                                <div class="col-12 col-md-6">
                                    <div class="input-group">
                                        <input type="password" name="password" value="<?php echo $row['password'] ?>" class="form-control" id="seepass" required>
                                        <button class="btn btn-secondary" type="button" id="seepass" onclick="myFunction()"> Lihat Password </button>
                                    </div>
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col col-md-2">
                                    <label for="text-input" class="form-control-label">Role</label>
                                </div>
                                <div class="col-12 col-md-6">
                                    <select name="role" id="select" class="form-control" required>
                                        <option value="<?php echo $row['role_pengguna'] ?>" selected><?php echo $row['role_pengguna'] ?></option>
                                        <option disabled>- Pilih Role -</option>
                                        <option value="SKPD">SKPD</option>
                                        <option value="Penyelia">Penyelia</option>
                                        <option value="Admin">Admin</option>
                                    </select>
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col col-md-2">
                                    <label for="text-input" class="form-control-label">Terakhir Login</label>
                                </div>
                                <div class="col-12 col-md-6">
                                    <?php
                                    if ($row['last_login'] == null){
                                        echo "Belum Pernah Login";
                                    } else {
                                        echo date('H:i j/m/Y', strtotime($row['last_login']));
                                        //echo $row['last_login'];
                                    }
                                    ?>
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col col-md-2">
                                </div>
                                <div class="col-12 col-md-6" align="right">
                                    <button type="submit" class="btn btn-primary" name="edit">
                                        <i class="fa fa-check"></i> Simpan Perubahan
                                    </button>
                                    <a class="btn btn-outline-secondary" href="index.php?contain=master_pengguna" role="button"><i class="fa fa-mail-reply"></i>&nbsp; Kembali</a>
                                </div>
                            </div>
                        </form>
                    </div>
                    <?php 
                        }
                    ?>
                </div>
            </div>


        </div>
    </div><!-- .animated -->
</div><!-- .content -->