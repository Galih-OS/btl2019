<div class="breadcrumbs">
    <div class="col-sm-4">
        <div class="page-header float-left">
            <div class="page-title">
                <h1>Master Realisasi</h1>
            </div>
        </div>
    </div>
    <div class="col-sm-8">
        <div class="page-header float-right">
        </div>
    </div>
</div>

<?php
    function NamaBulan($num) {
        $NamaBulan [1] = 'Januari';
        $NamaBulan [2] = 'Februari';
        $NamaBulan [3] = 'Maret';
        $NamaBulan [4] = 'April';
        $NamaBulan [5] = 'Mei';
        $NamaBulan [6] = 'Juni';
        $NamaBulan [7] = 'Juli';
        $NamaBulan [8] = 'Agustus';
        $NamaBulan [9] = 'September';
        $NamaBulan [10] = 'Oktober';
        $NamaBulan [11] = 'November';
        $NamaBulan [12] = 'Desember';
        $NamaBulan [13] = 'Gaji 13';
        $NamaBulan [14] = 'Gaji 14';

        return $NamaBulan[$num];
    }

    function NamaRealisasi($num) {
        $NamaRealisasi [1] = 'realisasi_januari';
        $NamaRealisasi [2] = 'realisasi_februari';
        $NamaRealisasi [3] = 'realisasi_maret';
        $NamaRealisasi [4] = 'realisasi_april';
        $NamaRealisasi [5] = 'realisasi_mei';
        $NamaRealisasi [6] = 'realisasi_juni';
        $NamaRealisasi [7] = 'realisasi_juli';
        $NamaRealisasi [8] = 'realisasi_agustus';
        $NamaRealisasi [9] = 'realisasi_september';
        $NamaRealisasi [10] = 'realisasi_oktober';
        $NamaRealisasi [11] = 'realisasi_november';
        $NamaRealisasi [12] = 'realisasi_desember';
        $NamaRealisasi [13] = 'realisasi_13';
        $NamaRealisasi [14] = 'realisasi_14';

        return $NamaRealisasi[$num];
    }
?>

<div class="content mt-3">
    <div class="animated fadeIn">
        <div class="row">

            <?php include "../include/connect.php"; ?>

            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <div class="float-left">
                            <strong class="card-title">Edit Realisasi</strong>
                        </div>
                    </div>
                    <?php 
                        // Jika Sukses
                        if(isset($_POST['simpan']))
                        {
                            $set = "";

                            foreach($db->query("SELECT DISTINCT status_gaji_13, status_gaji_14 FROM btl JOIN tahun ON btl.id_tahun = tahun.id_tahun
                                                WHERE tahun.status_tahun = 'Aktif'") as $row) {
                                if ($row['status_gaji_14'] == 'Aktif') {
                                    
                                    for ($i=1; $i <= 12; $i++) { 
                                        if ($i != 12) {
                                            $set .= NamaRealisasi($i)." = ".$_POST[NamaRealisasi($i)].",";
                                        } else {
                                            $set .= NamaRealisasi($i)." = ".$_POST[NamaRealisasi($i)].",";
                                        }
                                    }

                                    $set .= NamaRealisasi(13)." = ".$_POST[NamaRealisasi(13)].",";
                                    $set .= NamaRealisasi(14)." = ".$_POST[NamaRealisasi(14)];

                                } else if ($row['status_gaji_13'] == 'Aktif') {

                                    for ($i=1; $i <= 12; $i++) { 
                                        if ($i != 12) {
                                            $set .= NamaRealisasi($i)." = ".$_POST[NamaRealisasi($i)].",";
                                        } else {
                                            $set .= NamaRealisasi($i)." = ".$_POST[NamaRealisasi($i)].",";
                                        }
                                    }

                                    $set .= NamaRealisasi(13)." = ".$_POST[NamaRealisasi(13)];
                                }
                            }

                            $sql_update = $db->exec("UPDATE btl
                                                        SET ".$set."
                                                        WHERE id_skpd = '".$_GET["id_skpd"]."' AND id_rincian = '".$_GET["id_rincian"]."' ");
                            
                            if ($sql_update) {
                    ?>
                                <div class="alert  alert-success alert-dismissible fade show" role="alert">
                                    <span class="badge badge-pill badge-success">Sukses</span> Data Sukses Di Ubah.
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                    <?php
                            } else {
                    ?>
                                <div class="alert  alert-danger alert-dismissible fade show" role="alert">
                                    <span class="badge badge-pill badge-danger">Gagal</span> Data Gagal Di Ubah.
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                    <?php
                            }
                        }
                    ?>
                    <div class="card-body card-block">
                        <form action="" method="post" enctype="multipart/form-data" class="form-horizontal">
                        <?php 
                            $stmt = $db->prepare("SELECT nama_skpd FROM skpd WHERE id_skpd = '".$_GET["id_skpd"]."' ");
                            $stmt -> execute();
                            $invNum = $stmt -> fetch(PDO::FETCH_ASSOC);
                            $nama_skpd = $invNum['nama_skpd'];

                            $stmt = $db->prepare("SELECT nama_rincian FROM rincian WHERE id_rincian = '".$_GET["id_rincian"]."' ");
                            $stmt -> execute();
                            $invNum = $stmt -> fetch(PDO::FETCH_ASSOC);
                            $nama_rincian = $invNum['nama_rincian'];
                        ?>
                            <div align="center">
                                <h2><?php echo $nama_skpd; ?></h2>
                                <h2><?php echo $nama_rincian; ?></h2>
                            </div>
                            <br>
                    <?php
                        foreach($db->query('SELECT *
                                                FROM btl JOIN tahun ON btl.id_tahun = tahun.id_tahun
                                                WHERE id_skpd = "'.$_GET["id_skpd"].'" AND id_rincian = "'.$_GET["id_rincian"].'" ') as $row) {
                    ?>
                        <?php
                            $max = 0;
                            if ($row['status_gaji_14'] == 'Aktif') {
                                $max = 14;
                            } else if ($row['status_gaji_13'] == 'Aktif') {
                                $max = 13;
                            } else {
                                $max = 12;
                            }

                            for ($i=1; $i <= $max; $i++) {
                        ?>
                            <div class="row form-group" align="right">
                                <div class="col-2 col-md-2">
                                    <label for="text-input" class="form-control-label"><?php echo NamaBulan($i);?></label>
                                </div>
                                <div class="col-10 col-md-6">
                                    <input type="number" id="text-input" name="<?php echo NamaRealisasi($i);?>"
                                        value="<?php echo $row[NamaRealisasi($i)]; ?>" class="form-control">
                                </div>
                            </div>
                        <?php
                            }
                        ?>
                    <?php 
                        }
                    ?>
                            <div align="center">
                                <button type="submit" class="btn btn-primary" name="simpan">
                                    <i class="fa fa-check"></i> Simpan
                                </button>
                                <a class="btn btn-outline-secondary" href="index.php?contain=master_realisasi" role="button"><i class="fa fa-mail-reply"></i>&nbsp; Kembali</a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>


        </div>
    </div><!-- .animated -->
</div><!-- .content -->