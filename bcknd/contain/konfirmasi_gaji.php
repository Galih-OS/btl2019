<div class="breadcrumbs">
    <div class="col-sm-4">
        <div class="page-header float-left">
            <div class="page-title">
                <h1>Konfirmasi Gaji</h1>
            </div>
        </div>
    </div>
    <div class="col-sm-8">
        <div class="page-header float-right">
        </div>
    </div>
</div>

<div class="content mt-3">
    <div class="animated fadeIn">
        <div class="row">

            <?php include "../include/connect.php"; ?>

            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <div class="float-left">
                            <strong class="card-title">Edit Konfirmasi Gaji</strong>
                        </div>
                    </div>
                    <?php
                        if(isset($_POST['edit']))
                        {
                            if ($_GET["about"] == 'status_gaji_14' && $_GET["change"] == 'Aktif'){
                                $sql_update = $db->exec("UPDATE tahun
                                                            SET ".$_GET["about"]." = '".$_GET["change"]."' ,
                                                                status_gaji_13 = '".$_GET["change"]."'
                                                            WHERE id_tahun = ".$_GET["id"]." ");
                            } else {
                                $sql_update = $db->exec("UPDATE tahun
                                                            SET ".$_GET["about"]." = '".$_GET["change"]."'
                                                            WHERE id_tahun = ".$_GET["id"]." ");
                            }

                            if ($sql_update) {
                    ?>
                                <div class="alert  alert-success alert-dismissible fade show" role="alert">
                                    <span class="badge badge-pill badge-success">Sukses</span> Sukses Di Ubah.
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                    <?php
                            } else {
                    ?>
                                <div class="alert  alert-danger alert-dismissible fade show" role="alert">
                                    <span class="badge badge-pill badge-danger">Gagal</span> Gagal Di Ubah.
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                    <?php
                            }
                        }
                    ?>
                    <?php
                        foreach($db->query('SELECT *
                                                FROM tahun
                                                WHERE id_tahun = '.$_GET["id"].'') as $row) {
                    ?>
                    <div class="card-body card-block">
                        <form action="" method="post" enctype="multipart/form-data" class="form-horizontal">
                            <div class="row form-group">
                                <div class="col col-md-2">
                                </div>
                                <div class="col-12 col-md-6">
                                    <input type="text" id="text-input" name="status" value="<?php echo $row['id_tahun'] ?>" class="form-control" hidden>
                                </div>
                            </div>
                            <div align="center">
                                <h2 style="color:red;"><b>PERHATIAN..!!!</b></h2>
                                <h2>- Mengaktifkan gaji mengakibatkan terbuka/tutupnya kolom Gaji ke 13/14<br/>
                                    - Pastikan data Gaji ke 13/14 sudah terisi<br>
                                    - Bila Gaji ke 14 Aktif, gaji 13 otomatis Terbuka</h2>
                            </div>
                            <br/>
                            <div align="center">
                                <button type="submit" class="btn btn-primary" name="edit">
                                    <i class="fa fa-check"></i> Ya, Saya Mengerti
                                </button>
                                <a class="btn btn-outline-secondary" href="index.php?contain=setting_gaji" role="button"><i class="fa fa-mail-reply"></i>&nbsp; Kembali</a>
                            </div>
                        </form>
                    </div>
                    <?php 
                        }
                    ?>
                </div>
            </div>


        </div>
    </div><!-- .animated -->
</div><!-- .content -->