<div class="breadcrumbs">
    <div class="col-sm-4">
        <div class="page-header float-left">
            <div class="page-title">
                <h1>Laporan</h1>
            </div>
        </div>
    </div>
    <div class="col-sm-8">
        <div class="page-header float-right">
        </div>
    </div>
</div>

<?php
    function NamaBulan($num) {
        $NamaBulan [1] = 'Januari';
        $NamaBulan [2] = 'Februari';
        $NamaBulan [3] = 'Maret';
        $NamaBulan [4] = 'April';
        $NamaBulan [5] = 'Mei';
        $NamaBulan [6] = 'Juni';
        $NamaBulan [7] = 'Juli';
        $NamaBulan [8] = 'Agustus';
        $NamaBulan [9] = 'September';
        $NamaBulan [10] = 'Oktober';
        $NamaBulan [11] = 'November';
        $NamaBulan [12] = 'Desember';
        $NamaBulan [13] = 'Gaji 13';
        $NamaBulan [14] = 'Gaji 14';

        return $NamaBulan[$num];
    }

    function NamaRealisasi($num) {
        $NamaRealisasi [1] = 'realisasi_januari';
        $NamaRealisasi [2] = 'realisasi_februari';
        $NamaRealisasi [3] = 'realisasi_maret';
        $NamaRealisasi [4] = 'realisasi_april';
        $NamaRealisasi [5] = 'realisasi_mei';
        $NamaRealisasi [6] = 'realisasi_juni';
        $NamaRealisasi [7] = 'realisasi_juli';
        $NamaRealisasi [8] = 'realisasi_agustus';
        $NamaRealisasi [9] = 'realisasi_september';
        $NamaRealisasi [10] = 'realisasi_oktober';
        $NamaRealisasi [11] = 'realisasi_november';
        $NamaRealisasi [12] = 'realisasi_desember';
        $NamaRealisasi [13] = 'realisasi_13';
        $NamaRealisasi [14] = 'realisasi_14';

        return $NamaRealisasi[$num];
    }
?>

<div class="content mt-3">
    <div class="animated fadeIn">
        <div class="row">

            <?php include "../include/connect.php"; ?>

            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <div class="float-left">
                            <strong class="card-title">Laporan Realisasi</strong>
                        </div>
                        <div class="float-right">
                            <!-- <a class="btn btn-primary" href="index.php?contain=tambah_wp" role="button"><i class="fa fa-plus"></i>&nbsp; Tambah Tahun</a> -->
                        </div>
                    </div>

                    <div class="card-body" style="overflow-x:auto;">

                        <div class="tab-content pl-3 pt-2" id="nav-tabContent">

                                <form action="" method="post" enctype="multipart/form-data" class="form-horizontal">
                                    <div class="row form-group" align="right">
                                        <div class="col-2 col-md-2">
                                            <label for="text-input" class="form-control-label">Nama SKPD</label>
                                        </div>
                                        <div class="col-10 col-md-6">
                                            <select class="form-control" name="cari" onchange="this.form.submit();">
                                        <?php
                                            if(isset($_POST['cari'])) {
                                                echo "<option disabled selected>".$_POST['cari']."</option>";
                                            } else {
                                                echo "<option disabled selected>- Pilih SKPD -</option>";
                                            }
                                             
                                            foreach($db->query('SELECT id_skpd, nama_skpd FROM skpd ORDER BY nama_skpd ASC') as $row) {
                                        ?>
                                                <option value="<?php echo $row['nama_skpd']; ?>"><?php echo $row['nama_skpd']; ?></option>
                                        <?php
                                            }
                                        ?>
                                            </select>
                                        </div>
                                    </div>
                                </form>
                                <!-- End Contain Cari Data -->
                                <!-- End Contain Cari Data -->
                            <?php 
                            ?>
                                <!-- TABLE DATA -->
                                <!-- TABLE DATA -->
                                <form action="" method="post" enctype="multipart/form-data">
                                    <table id="bootstrap-data-table-export" class="table table-striped table-hover table-bordered">
                                        <thead>
                                            <tr align="center">
                                                <th>Nama SKPD</th>
                                                <th>Kode Rincian</th>
                                                <th>Nama Rincian</th>
                                            <?php
                                                for ($i=1; $i <= 14; $i++) { 
                                                    echo "<th>".NamaBulan($i)."</th>";
                                                }
                                            ?>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        <?php 
                                        if(isset($_POST['cari'])) {
                                            $nama_skpd = $_POST['cari'];
                                            foreach($db->query('SELECT nama_skpd, rincian.id_rincian as id_rincian, anggaran, nama_rincian,
                                                                        realisasi_januari, realisasi_februari, realisasi_maret, realisasi_april,
                                                                        realisasi_mei, realisasi_juni, realisasi_juli, realisasi_agustus, realisasi_september,
                                                                        realisasi_oktober, realisasi_november, realisasi_desember, realisasi_13, realisasi_14
                                                                FROM btl
                                                                JOIN skpd ON skpd.id_skpd = btl.id_skpd
                                                                JOIN rincian ON rincian.id_rincian = btl.id_rincian
                                                                JOIN tahun ON tahun.id_tahun = btl.id_tahun
                                                                WHERE skpd.nama_skpd = "'.$nama_skpd.'" AND tahun.status_tahun = "Aktif" ') as $row) {
                                        ?>
                                            <tr>
                                                <td align="">
                                                    <?php echo $row['nama_skpd'];?>
                                                </td>
                                                <td align="center">
                                                    <?php echo $row['id_rincian'];?>
                                                </td>
                                                <td align="">
                                                    <?php echo $row['nama_rincian'];?>
                                                </td>
                                                <?php
                                                    for ($i=1; $i <= 14; $i++) {
                                                        echo "<td align='right'>".number_format($row[NamaRealisasi($i)])."</td>";
                                                    }
                                                ?>
                                            </tr>
                                        <?php
                                            }
                                        } else {
                                            /*foreach($db->query('SELECT nama_skpd, rincian.id_rincian as id_rincian, anggaran, nama_rincian,
                                                                        realisasi_januari, realisasi_februari, realisasi_maret, realisasi_april,
                                                                        realisasi_mei, realisasi_juni, realisasi_juli, realisasi_agustus, realisasi_september,
                                                                        realisasi_oktober, realisasi_november, realisasi_desember, realisasi_13, realisasi_14
                                                                FROM btl
                                                                JOIN skpd ON skpd.id_skpd = btl.id_skpd
                                                                JOIN rincian ON rincian.id_rincian = btl.id_rincian
                                                                JOIN tahun ON tahun.id_tahun = btl.id_tahun
                                                                WHERE tahun.status_tahun = "Aktif" ') as $row) {*/
                                        ?>
                                            <!-- <tr>
                                                <td align="center">
                                                    <?php echo $row['nama_skpd'];?>
                                                </td>
                                                <td align="center">
                                                    <?php echo $row['id_rincian'];?>
                                                </td>
                                                <td align="">
                                                    <?php echo $row['nama_rincian'];?>
                                                </td>
                                                <?php
                                                    for ($i=1; $i <= 14; $i++) {
                                                        echo "<td align='right'>".number_format($row[NamaRealisasi($i)])."</td>";
                                                    }
                                                ?>
                                            </tr> -->
                                        <?php
                                            //}
                                        }
                                        ?>
                                        </tbody>
                                    </table>
                                </form>

                        </div>

                    </div>
                </div>
            </div>


        </div>
    </div><!-- .animated -->
</div><!-- .content -->

<?php
    if(isset($_POST['hapus']))
    {
        $id_restoran = $_POST['id_restoran'];
        
        $stmt=$db->prepare("DELETE FROM restoran WHERE id_restoran = '".$id_restoran."'");
        $stmt->execute();

        
        if($stmt){
            echo '<script languange="javascript">window.alert("'.$stmt.'")</script>';
            //echo '<script languange="javascript">window.location="index.php?contain=master_wp"</script>';
        } else {
            //echo '<script languange="javascript">alert("'.$id_restoran.'")</script>';
        }

    }
?>