<div class="breadcrumbs">
    <div class="col-sm-4">
        <div class="page-header float-left">
            <div class="page-title">
                <h1>Master Anggaran</h1>
            </div>
        </div>
    </div>
    <div class="col-sm-8">
        <div class="page-header float-right">
        </div>
    </div>
</div>

<div class="content mt-3">
    <div class="animated fadeIn">
        <div class="row">

            <?php include "../include/connect.php"; ?>

            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <div class="float-left">
                            <strong class="card-title">Data Anggaran</strong>
                        </div>
                        <div class="float-right">
                            <!-- <a class="btn btn-primary" href="index.php?contain=tambah_wp" role="button"><i class="fa fa-plus"></i>&nbsp; Tambah Tahun</a> -->
                        </div>
                    </div>

                    <div class="card-body" style="overflow-x:auto;">

                        <nav>
                            <div class="nav nav-tabs" id="nav-tab" role="tablist">
                                <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">Anggaran</a>
                                <a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile" aria-selected="false">Upload Data</a>
                            </div>
                        </nav>
                        <div class="tab-content pl-3 pt-2" id="nav-tabContent">
                            <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
                                <!-- Contain Cari Data -->
                                <!-- Contain Cari Data -->
                                <br>
                                <form action="" method="post" enctype="multipart/form-data" class="form-horizontal">
                                    <div class="row form-group" align="right">
                                        <div class="col-2 col-md-2">
                                            <label for="text-input" class="form-control-label">Nama SKPD</label>
                                        </div>
                                        <div class="col-10 col-md-6">
                                            <select class="form-control" name="cari" onchange="this.form.submit();">
                                        <?php
                                            if(isset($_POST['cari'])) {
                                                echo "<option disabled selected>".$_POST['cari']."</option>";
                                            } else {
                                                echo "<option disabled selected>- Pilih SKPD -</option>";
                                            }
                                             
                                            foreach($db->query('SELECT id_skpd, nama_skpd FROM skpd ORDER BY nama_skpd ASC') as $row) {
                                        ?>
                                                <option value="<?php echo $row['nama_skpd']; ?>"><?php echo $row['nama_skpd']; ?></option>
                                        <?php
                                            }
                                        ?>
                                            </select>
                                        </div>
                                    </div>
                                </form>
                                <!-- End Contain Cari Data -->
                                <!-- End Contain Cari Data -->
                            <?php 
                            ?>
                                <!-- TABLE DATA -->
                                <!-- TABLE DATA -->
                                <form action="" method="post" enctype="multipart/form-data">
                                    <table id="bootstrap-data-table-export" class="table table-striped table-hover table-bordered">
                                        <thead>
                                            <tr align="center">
                                                <th>Nama SKPD</th>
                                                <th>Kode Rincian</th>
                                                <th>Nama Rincian</th>
                                                <th>Anggaran</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        <?php 
                                        if(isset($_POST['cari'])) {
                                            $nama_skpd = $_POST['cari'];
                                            foreach($db->query('SELECT nama_skpd, rincian.id_rincian as id_rincian, anggaran, nama_rincian FROM btl
                                                                JOIN skpd ON skpd.id_skpd = btl.id_skpd
                                                                JOIN rincian ON rincian.id_rincian = btl.id_rincian
                                                                JOIN tahun ON tahun.id_tahun = btl.id_tahun
                                                                WHERE skpd.nama_skpd = "'.$nama_skpd.'" AND tahun.status_tahun = "Aktif" ') as $row) {
                                        ?>
                                            <tr>
                                                <td align="center">
                                                    <?php echo $row['nama_skpd'];?>
                                                </td>
                                                <td align="center">
                                                    <?php echo $row['id_rincian'];?>
                                                </td>
                                                <td align="">
                                                    <?php echo $row['nama_rincian'];?>
                                                </td>
                                                <td align="right">
                                                    <?php echo number_format($row['anggaran']);?>
                                                </td>
                                            </tr>
                                        <?php
                                            }
                                        } else {
                                            foreach($db->query('SELECT nama_skpd, rincian.id_rincian as id_rincian, anggaran, nama_rincian FROM btl
                                                                JOIN skpd ON skpd.id_skpd = btl.id_skpd
                                                                JOIN rincian ON rincian.id_rincian = btl.id_rincian
                                                                JOIN tahun ON tahun.id_tahun = btl.id_tahun
                                                                WHERE tahun.status_tahun = "Aktif" ') as $row) {
                                        ?>
                                            <tr>
                                                <td align="center">
                                                    <?php echo $row['nama_skpd'];?>
                                                </td>
                                                <td align="center">
                                                    <?php echo $row['id_rincian'];?>
                                                </td>
                                                <td align="">
                                                    <?php echo $row['nama_rincian'];?>
                                                </td>
                                                <td align="right">
                                                    <?php echo number_format($row['anggaran']);?>
                                                </td>
                                            </tr>
                                        <?php
                                            }
                                        }
                                        ?>
                                        </tbody>
                                    </table>
                                </form>
                                <!-- END TABLE DATA -->
                                <!-- END TABLE DATA -->
                            </div>
                            <div class="tab-pane fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">
                                <!-- Contain Upload Data -->
                                <!-- Contain Upload Data -->
                                <br>
                                <form action="contain/upload_anggaran.php" method="post" enctype="multipart/form-data" class="form-horizontal">
                                    <div class="row form-group" align="right">
                                        <div class="col-2 col-md-2">
                                            <label for="text-input" class="form-control-label">Upload Data Anggaran</label>
                                        </div>
                                        <div class="col-10 col-md-6">
                                            <input class="form-control-file" name="fileexcel" type="file" required>
                                        </div>
                                    </div>
                                    <div class="row form-group">
                                        <div class="col col-md-2">
                                        </div>
                                        <div class="col-12 col-md-6" align="left">
                                            *File Berupa Excel Berformat .xls
                                        </div>
                                    </div>
                                    <div class="row form-group" align="right">
                                        <div class="col-2 col-md-2">
                                            <label for="text-input" class="form-control-label">Tahun Anggaran</label>
                                        </div>
                                        <div class="col-10 col-md-6">
                                            <select class="form-control" name="tahun" required>
                                        <?php 
                                            foreach($db->query('SELECT * FROM tahun ORDER BY status_tahun ASC') as $row) {
                                        ?>
                                                <option value="<?php echo $row['id_tahun']; ?>"><?php echo $row['nama_tahun']; ?></option>';
                                        <?php
                                            }
                                        ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="row form-group">
                                        <div class="col col-md-2">
                                        </div>
                                        <div class="col-12 col-md-6" align="left">
                                            <button type="submit" class="btn btn-success btn-sm" name="tambah">
                                                <i class="fa fa-arrow-circle-up"></i> Upload
                                            </button>
                                        </div>
                                    </div>
                                </form>
                                <!-- End Contain Upload Data -->
                                <!-- End Contain Upload Data -->
                            </div>
                        </div>

                    </div>
                </div>
            </div>


        </div>
    </div><!-- .animated -->
</div><!-- .content -->

<?php
    if(isset($_POST['hapus']))
    {
        $id_restoran = $_POST['id_restoran'];
        
        $stmt=$db->prepare("DELETE FROM restoran WHERE id_restoran = '".$id_restoran."'");
        $stmt->execute();

        
        if($stmt){
            echo '<script languange="javascript">window.alert("'.$stmt.'")</script>';
            //echo '<script languange="javascript">window.location="index.php?contain=master_wp"</script>';
        } else {
            //echo '<script languange="javascript">alert("'.$id_restoran.'")</script>';
        }

    }
?>