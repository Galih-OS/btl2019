<div class="breadcrumbs">
    <div class="col-sm-4">
        <div class="page-header float-left">
            <div class="page-title">
                <h1>Setting Gaji 13/14</h1>
            </div>
        </div>
    </div>
    <div class="col-sm-8">
        <div class="page-header float-right">
        </div>
    </div>
</div>

<div class="content mt-3">
    <div class="animated fadeIn">
        <div class="row">

            <?php include "../include/connect.php"; ?>

            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <div class="float-left">
                            <strong class="card-title">Status Gaji 13/14</strong>
                        </div>
                        <div class="float-right">
                            <!-- <a class="btn btn-primary" href="index.php?contain=tambah_wp" role="button"><i class="fa fa-plus"></i>&nbsp; Tambah Tahun</a> -->
                        </div>
                    </div>

                    <div class="card-body" style="overflow-x:auto;">
                        <!-- TABLE DATA -->
                        <!-- TABLE DATA -->
                        <!-- TABLE DATA -->
                        <form action="" method="post" enctype="multipart/form-data">
                            <table id="bootstrap-data-table-export" class="table table-hover table-bordered">
                                <thead>
                                    <tr align="center">
                                        <th colspan="2">Realisasi Gaji ke 13</th>
                                        <th colspan="2">Realisasi Gaji ke 14</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php 
                                    foreach($db->query('SELECT * FROM tahun WHERE status_tahun = "Aktif" ') as $row) {
                                ?>
                                        <div align="center">
                                            <h2>Realisasi Gaji Tahun <?php echo$row['nama_tahun']; ?></h2>
                                            <br>
                                        </div>
                                    <tr>
                                        <td align="center">
                                            <?php
                                                if($row['status_gaji_13']=='Aktif'){
                                                    echo '<span class="badge badge-pill badge-success">Aktif</span>';
                                                } else {
                                                    echo '<span class="badge badge-pill badge-secondary">Tidak Aktif</span>';
                                                }   
                                            ?>
                                        </td>
                                        <td align="center">
                                    <?php
                                        if($row['status_gaji_13']=='Aktif'){
                                            echo '<a class="btn btn-warning btn-sm" href="index.php?contain=konfirmasi_gaji&id='.$row['id_tahun'].'&about=status_gaji_13&change=Tidak Aktif" role="button"><i class="fa fa-edit"></i>&nbsp; Jadikan Tidak Aktif</a>';
                                        } else {
                                            echo '<a class="btn btn-info btn-sm" href="index.php?contain=konfirmasi_gaji&id='.$row['id_tahun'].'&about=status_gaji_13&change=Aktif" role="button"><i class="fa fa-edit"></i>&nbsp; Jadikan Aktif</a>';
                                        }
                                    ?>
                                        </td>
                                        <td align="center">
                                            <?php
                                                if($row['status_gaji_14']=='Aktif'){
                                                    echo '<span class="badge badge-pill badge-success">Aktif</span>';
                                                } else {
                                                    echo '<span class="badge badge-pill badge-secondary">Tidak Aktif</span>';
                                                }
                                            ?>
                                        </td>
                                        <td align="center">
                                    <?php
                                        if($row['status_gaji_14']=='Aktif'){
                                            echo '<a class="btn btn-warning btn-sm" href="index.php?contain=konfirmasi_gaji&id='.$row['id_tahun'].'&about=status_gaji_14&change=Tidak Aktif" role="button"><i class="fa fa-edit"></i>&nbsp; Jadikan Tidak Aktif</a>';
                                        } else {
                                            echo '<a class="btn btn-info btn-sm" href="index.php?contain=konfirmasi_gaji&id='.$row['id_tahun'].'&about=status_gaji_14&change=Aktif" role="button"><i class="fa fa-edit"></i>&nbsp; Jadikan Aktif</a>';
                                        }
                                    ?>
                                        </td>
                                    </tr>
                                <?php
                                    }
                                ?>
                                </tbody>
                            </table>
                        </form>
                        <!-- END TABLE DATA -->
                        <!-- END TABLE DATA -->
                        <!-- END TABLE DATA -->
                    </div>
                </div>
            </div>


        </div>
    </div><!-- .animated -->
</div><!-- .content -->

<?php
    if(isset($_POST['hapus']))
    {
        $id_restoran = $_POST['id_restoran'];
        
        $stmt=$db->prepare("DELETE FROM restoran WHERE id_restoran = '".$id_restoran."'");
        $stmt->execute();

        
        if($stmt){
            echo '<script languange="javascript">window.alert("'.$stmt.'")</script>';
            //echo '<script languange="javascript">window.location="index.php?contain=master_wp"</script>';
        } else {
            //echo '<script languange="javascript">alert("'.$id_restoran.'")</script>';
        }

    }
?>